#include <veb/set.hpp>
#include <limits>

int main() {
	
	veb::set<std::numeric_limits<size_t>::max()> x;
	auto m = x.first();
	if (m) {
		std::cout << "min should be none\n";
		return EXIT_FAILURE;
	}

	x.insert(45);
	m = x.first();
	if (!m) {
		return EXIT_FAILURE;
	}
	if (*m != 45) {
		return EXIT_FAILURE;
	}

	for (size_t i = 0; i < 10; ++i) {
		x.insert(5 + i * 11);
		m = x.first();
		if (!m) {
			return EXIT_FAILURE;
		}
		if (*m != 5) {
			std::cout << "min is " << *m << " instead of " << 5 << "\n";
			return EXIT_FAILURE;
		}
	}

	for (size_t i = 0; i < 10; ++i) {
		x.insert(3 + i * 11);
		m = x.first();
		if (!m) {
			return EXIT_FAILURE;
		}
		if (*m != 3) {
			std::cout << "min is " << *m << " instead of " << 3 << "\n";
			return EXIT_FAILURE;
		}
	}

	return EXIT_SUCCESS;
}