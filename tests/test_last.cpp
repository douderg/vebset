#include <veb/set.hpp>
#include <limits>

int main() {
	
	veb::set<std::numeric_limits<size_t>::max()> x;

	x.insert(5);
	auto m = x.last();
	if (!m) {
		return EXIT_FAILURE;
	}
	if (*m != 5) {
		return EXIT_FAILURE;
	}

	x.insert(16);
	m = x.last();
	if (!m) {
		return EXIT_FAILURE;
	}
	if (*m != 16) {
		return EXIT_FAILURE;
	}

	x.insert(2);
	m = x.last();
	if (!m) {
		return EXIT_FAILURE;
	}
	if (*m != 16) {
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}