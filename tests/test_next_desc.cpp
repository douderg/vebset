#include <veb/set.hpp>
#include <iostream>
#include <optional>

int main() {

	veb::set<std::numeric_limits<size_t>::max(), std::greater> x;
	size_t last = 2048;
	size_t step = 3;
	size_t inserted = 0;
	for (size_t i = 0; i < last; i += step) {
		if (!x.insert(i)) {
			std::cout << "error: could not insert " << i << "\n";
			return EXIT_FAILURE;
		} else {
			++inserted;
		}
	}

	for (size_t i = last - (last % step); i + step >= step; i -= step) {
		if (x.insert(i)) {
			std::cout << "error: inserted " << i << "\n";
			return EXIT_FAILURE;
		}
	}

	auto next = x.first();
	if (!next.has_value()) {
		std::cout << "error: first is none\n";
		return EXIT_FAILURE;
	}
	
	auto previous = *next;
	next = x.next(previous);

	size_t count = 1;
	while (next.has_value()) {
		auto value = *next;
		if (value >= previous) {
			std::cout << "error: " << previous << " should be less than " << value << "\n";
		}
		++count;
		if (value.to_ullong() % step != 0) {
			std::cout << "error: visited " << value << "\n";
			return EXIT_FAILURE;
		}
		previous = value;
		next = x.next(value);
	}

	if (count != inserted) {
		std::cout << "count is " << count << "\n";
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}