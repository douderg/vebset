#include <veb/set.hpp>
#include <iostream>

int main(int argc, char** argv) {

	veb::set<std::numeric_limits<size_t>::max()> x;

	size_t n = 1024;
	size_t s1 = 3;
	size_t s2 = 7;
	for (size_t i = 0; i < n; i += s1) {
		if (!x.insert(i)) {
			std::cout << "inserting " << i << "\n";
			return EXIT_FAILURE;
		}
	}

	for (size_t i = 0; i < n; i += s2) {
		x.remove(i);
	}

	for (size_t i = 0; i < n; ++i) {
		std::cout << "checking for " << i << "\n";
		if (i % s1 != 0 || i % s2 == 0) {
			if (x.contains(i)) {
				return EXIT_FAILURE;
			}
		} else {
			if (!x.contains(i)) {
				return EXIT_FAILURE;
			}
		}
	}

	auto next = x.first();
	if (!next.has_value()) {
		std::cout << "error: first is none\n";
		return EXIT_FAILURE;
	}
	
	auto previous = *next;
	next = x.next(previous);

	size_t count = 0;
	while (next.has_value()) {
		auto value = *next;
		if (value <= previous) {
			std::cout << "error: " << previous << " should be less than " << value << "\n";
		}
		++count;
		if (value.to_ullong() % s1 != 0 || value.to_ullong() % s2 == 0) {
			std::cout << "error: visited " << value << "\n";
			return EXIT_FAILURE;
		}
		previous = value;
		next = x.next(value);
	}

	return EXIT_SUCCESS;
}