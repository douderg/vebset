#include <veb/set.hpp>
#include <iostream>
#include <limits>

int main() {
	
	veb::set<std::numeric_limits<size_t>::max()> x;

	for (size_t i = 0; i < 64; i+=3) {
		if (!x.insert(i)) {
			std::cout << "inserting " << i << "\n";
			return EXIT_FAILURE;
		}
	}

	for (size_t i = 0; i < 64; ++i) {
		std::cout << "checking for " << i << "\n";
		if (i%3 != 0) {
			if (x.contains(i)) {
				return EXIT_FAILURE;
			}
		} else {
			if (!x.contains(i)) {
				return EXIT_FAILURE;
			}
		}
	}

	return EXIT_SUCCESS;
}



