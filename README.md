## Overview ##

This is a header only library providing a Van Emde Boas tree implementation in C++. These trees can store ordered keys while providing O(log(log(n)))
time complexity for most operations. They are not widespread because of their overall complexity and space requirements. The goal of this
project is mainly to satisfy my curiosity on what the outcome will be if one applies template programming techniques for the implementation of a
recursively defined data structure.

This implementation is based on the reduced space VEB-trees described in [Introduction to Algorithms, Third Edition](https://mitpress.mit.edu/books/introduction-algorithms-third-edition) 
along with some tweaks to further reduce memory usage by switching to a dense representation for low rank subtrees during compilation. The maximum capacity of each tree is
defined statically during creation (they cannot be dynamically expanded during runtime).

## Basic example ##

A basic example using ascending order:

```
#include <veb/set.hpp>
#include <numeric>
#include <iostream>

int main() {
    veb::set<std::numeric_limits<size_t>::max()> tree;  // constructs a tree capable of fitting up to 2^(8 * sizeof(size_t)) entries
       
    tree.insert(15); // integral keys are implicitly converted to an appropriately sized bitset
    tree.insert(12);
       
    auto first = tree.first();
    std::cout << *first << " " << first->to_ulong() << "\n";

    auto second = tree.next(*first);
    std::cout << *second << " " << second->to_ulong() << "\n";

    return EXIT_SUCCESS;
}
```

For a different ordering one needs to simply pass a custom comparator for the second template parameter

```
veb::set<std::numeric_limits<size_t>::max(), std::greater>
```

**Note:** STL bitsets are not orderable by default, specializations for std::less&lt;bitset&lt;N&gt;&gt; and std::greater&lt;bitset&lt;N&gt;&gt; are defined in the header veb/utils.hpp
(which is included by veb/set.hpp). These definitions may or may not conflict with your project, so consider yourselves warned if you ever decide to use this library.
As a matter of fact, this library is (and probably will remain) experimental so my advise is to avoid using it for anything else other than research.

## Complexity ##

The space complexity is O(N) for the average case and O(U) for the worst case (when the N inserted keys are uniformly distributed across the entire universe size U). Each insertion may allocate a dense subtree that can store a number of subsequent keys that fall within its range. If the distribution of the inserted keys is efficiently sparse, then each key might correspond to an entire subtree. The library uses a compile-time check to limit the maximum size of each dense subtree so that there is no additional space overhead compared to a pure sparse implementation for the average case.

Operation | Complexity
--- | ---
Insertion | O(log(log(U)))
Search for specific key | O(log(log(U)))
Removal | O(log(log(U)))
Find first key | O(1)
Find last key | O(1)
Find next key | O(log(log(U)))
Find previous key | O(log(log(U)))

## Todos ##

- add test cases and fix any issues that may arise
- provide STL-like iterators via `begin` and `end` methods
- stabilize public interface of `veb::set`
- provide `veb::map` implementation for storing satellite data for each key in the tree
