#ifndef _VEB_UTILS_HPP_
#define _VEB_UTILS_HPP_

#include <bitset>

namespace veb {


namespace utils {

template <class Impl, class... Impls>
struct select_impl {
	typedef typename std::conditional<
		Impl::is_enabled,
		Impl,
		typename select_impl<Impls...>::type>::type type;
};

template <class Impl>
struct select_impl<Impl> {
	typedef typename std::enable_if<
		Impl::is_enabled,
		Impl>::type type;
};

template <
	size_t N, 
	template <size_t, class...> class LhsStaticLayout, 
	template <size_t, class...> class LhsImpl,
	template <size_t, class...> class RhsStaticLayout,
	template <size_t, class...> class RhsImpl,
	class...Ignored>
struct node_impl_selection {
	static constexpr bool is_less_or_equal = sizeof(LhsStaticLayout<N, Ignored...>) <= sizeof(RhsStaticLayout<N, Ignored...>);
	typedef typename std::conditional<is_less_or_equal, LhsImpl<N, Ignored...>, RhsImpl<N, Ignored...>>::type type;
};

}


namespace bits {

template <size_t N>
struct bitset_less_ulong {
	constexpr static bool is_enabled = N <= std::numeric_limits<unsigned long>::digits;
	bool operator()(const std::bitset<N>&, const std::bitset<N>&) const;
};

template <size_t N>
struct bitset_less_ullong {
	constexpr static bool is_enabled = N > std::numeric_limits<unsigned long>::digits && N <= std::numeric_limits<unsigned long long>::digits;
	bool operator()(const std::bitset<N>&, const std::bitset<N>&) const;
};

template <size_t N>
struct bitset_less_arbitrary {
	constexpr static bool is_enabled = true;
	bool operator()(const std::bitset<N>&, const std::bitset<N>&) const;
};

template <size_t N>
class bitset_less {
public:
	bool operator()(const std::bitset<N>& x, const std::bitset<N>& y) const;
private:
	template <class S, class...Ss>
	struct select_impl {
		typedef typename std::conditional<S::is_enabled, S, typename select_impl<Ss...>::type>::type type;
	};

	template <class S>
	struct select_impl<S> {
		typedef typename std::enable_if<S::is_enabled, S>::type type;
	};

	typename select_impl<bitset_less_ulong<N>, bitset_less_ullong<N>, bitset_less_arbitrary<N>>::type impl_;
};

template <size_t N>
bool bitset_less_ulong<N>::operator()(const std::bitset<N>& x, const std::bitset<N>& y) const {
	return x.to_ulong() < y.to_ulong();
}

template <size_t N>
bool bitset_less_ullong<N>::operator()(const std::bitset<N>& x, const std::bitset<N>& y) const {
	return x.to_ullong() < y.to_ullong();
}

template <size_t N>
bool bitset_less_arbitrary<N>::operator()(const std::bitset<N>& x, const std::bitset<N>& y) const {
	for (int i = N - 1; i >=0; --i) {
		if (x[i] ^ y[i]) {
			return y[i];
		}
	}
	return false;
}

template <size_t N>
bool bitset_less<N>::operator()(const std::bitset<N>& x, const std::bitset<N>& y) const {
	return impl_(x, y);
}

template <size_t N>
struct count {
	static constexpr size_t value = ((N & 1) == 1) + count<(N >> 1)>::value;
};

template <>
struct count<0> {
	static constexpr size_t value = 0;
};

}

}

namespace std {

template <size_t N>
bool operator<(const std::bitset<N>& x, const std::bitset<N>& y) {
	return veb::bits::bitset_less<N>()(x, y);
}

template <size_t N>
bool operator>(const std::bitset<N>& x, const std::bitset<N>& y) {
	return veb::bits::bitset_less<N>()(y, x);
}

template <size_t N>
bool operator<=(const std::bitset<N>& x, const std::bitset<N>& y) {
	return !veb::bits::bitset_less<N>()(y, x);
}

template <size_t N>
bool operator>=(const std::bitset<N>& x, const std::bitset<N>& y) {
	return !veb::bits::bitset_less<N>()(x, y);
}

}

#endif