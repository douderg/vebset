#ifndef _VEB_SET_HPP_
#define _VEB_SET_HPP_

#include <veb/utils.hpp>
#include <optional>
#include <cmath>
#include <array>
#include <memory>
#include <unordered_map>
#include <iostream>
#include <stack>
#include <tuple>

namespace veb {

template <size_t N, template <class> class C = std::less>
class set {
	
	static constexpr size_t root_bits = (bits::count<N>::value != 1) ? ceil(log2(N)) : log2(N);
	class node;

public:
	typedef std::bitset<root_bits> key_t;
	typedef typename node::iterator iterator;

	std::optional<key_t> first() const;
	std::optional<key_t> last() const;
	bool insert(const key_t&);
	bool contains(const key_t&) const;
	std::optional<key_t> next(const key_t&) const;
	std::optional<key_t> previous(const key_t&) const;
	void remove(const key_t&);
	bool empty() const;

	void diag(const key_t&);
private:
	node inner_;
};


template <size_t N, template <class> class C>
class set<N, C>::node {

	template <size_t U, class...O> class average_size_of_sparse_impl;
	template <size_t U, class...O> class average_size_of_dense_impl;
	template <size_t U, class...O> class sparse_impl;
	template <size_t U, class...O> class dense_impl;

public:

	static constexpr size_t key_bits = root_bits;
	static constexpr size_t min_rank = 1;
	static constexpr size_t byte_size = 8;

	typedef typename sparse_impl<key_bits>::iterator iterator;

	std::optional<key_t> first() const;
	std::optional<key_t> last() const;
	bool insert(const key_t&);
	bool contains(const key_t&) const;
	std::optional<key_t> next(const key_t&) const;
	std::optional<key_t> previous(const key_t&) const;
	void remove(const key_t&);
	bool empty() const;

	void diag(const key_t&);

private:
	sparse_impl<key_bits> inner_;
};

template <size_t N, template <class> class C>
template <size_t U, class...O>
class set<N, C>::node::sparse_impl {
public:

	static constexpr size_t bits = U;
	static constexpr size_t summary_bits = ceil(bits * 0.5), cluster_bits = floor(bits * 0.5);
	static constexpr size_t summary_size = exp2(summary_bits), cluster_size = exp2(cluster_bits);

	template <class T>
	class cluster_storage;

	class iterator;

	typedef typename utils::node_impl_selection<
		summary_bits, 
		average_size_of_sparse_impl, 
		sparse_impl, 
		average_size_of_dense_impl, 
		dense_impl>::type summary_t;

	typedef typename utils::node_impl_selection<
		cluster_bits, 
		average_size_of_sparse_impl, 
		sparse_impl, 
		average_size_of_dense_impl, 
		dense_impl>::type cluster_t;

	sparse_impl();
	std::optional<key_t> first() const;
	std::optional<key_t> last() const;
	bool insert(const key_t&);
	bool contains(const key_t&) const;
	std::optional<key_t> next(const key_t&) const;
	std::optional<key_t> previous(const key_t&) const;
	void remove(const key_t&);
	bool empty() const;

	void diag(const key_t&);
private:
	std::optional<key_t> previous(const key_t&, size_t, size_t) const;

	bool contains_value_;
	key_t first_, last_;
	std::shared_ptr<summary_t> summary_;
	cluster_storage<cluster_t> cluster_;
};

template <size_t N, template <class> class C>
template <size_t U, class...O>
class set<N, C>::node::sparse_impl<U, O...>::iterator {

};

/* specialization of sparse_impl for min rank */

template <size_t N, template <class> class C>
template <class...O>
class set<N, C>::node::sparse_impl<set<N, C>::node::min_rank, O...> {
	static constexpr size_t bits = log2(set<N, C>::node::min_rank);
public:

	class iterator;
	
	std::optional<key_t> first() const;
	std::optional<key_t> last() const;
	bool insert(const key_t&);
	bool contains(const key_t&) const;
	std::optional<key_t> next(const key_t&) const;
	std::optional<key_t> previous(const key_t&) const;
	void remove(const key_t&);
	bool empty() const;

	void diag(const key_t&);
private:
	bool contains_value_;
	key_t first_, last_;
};

template <size_t N, template <class> class C>
template <class...O>
class set<N, C>::node::sparse_impl<set<N, C>::node::min_rank, O...>::iterator {

};

template <size_t N>
struct storage_bits {
	static constexpr size_t bits = 1 + 2 * N
		+  storage_bits<static_cast<size_t>(ceil(N * 0.5))>::bits
		+  static_cast<size_t>(exp2(ceil(N * 0.5))) * storage_bits<static_cast<size_t>(floor(N * 0.5))>::bits;
};

template <>
struct storage_bits<1> {
	static constexpr size_t bits = 3;
};

template <>
struct storage_bits<0> {
	static constexpr size_t bits = 0;
};

template <size_t...I>
constexpr std::array<size_t, sizeof...(I)> make_storage_bits(std::index_sequence<I...>) {
	return std::array<size_t, sizeof...(I)> { storage_bits<I>::bits... };
}


template <size_t N, template <class> class C>
template <size_t U, class...O>
class set<N, C>::node::dense_impl {
public:
	static constexpr size_t key_bits = U;
	static constexpr std::array<size_t, key_bits + 1> storage_bits = make_storage_bits(std::make_index_sequence<key_bits + 1>{});

	typedef std::bitset<storage_bits.back()> bits_t;

	class iterator;

	dense_impl();
	std::optional<key_t> first() const;
	std::optional<key_t> last() const;
	bool insert(const key_t&);
	bool contains(const key_t&) const;
	std::optional<key_t> next(const key_t&) const;
	std::optional<key_t> previous(const key_t&) const;
	void remove(const key_t&);
	bool empty() const;

	void diag(const key_t&);

private:
	bool insert(const key_t&, size_t, size_t);
	bool contains(const key_t&, size_t, size_t) const;
	std::optional<key_t> next(const key_t&, size_t, size_t) const;
	std::optional<key_t> previous(const key_t&, size_t, size_t) const;
	void remove(const key_t&, size_t, size_t);
	bits_t bits_;
};

template <size_t N, template <class> class C>
template <size_t U, class...O>
class set<N, C>::node::dense_impl<U, O...>::iterator {

};

/* specialization of dense_impl for min rank */

template <size_t N, template <class> class C>
template <class...O>
class set<N, C>::node::dense_impl<set<N, C>::node::min_rank, O...> {
public:
	class iterator;
	static constexpr size_t key_bits = set<N, C>::node::min_rank;
	static constexpr size_t storage_bits = 1 + 2 * key_bits;

	std::optional<key_t> first() const;
	std::optional<key_t> last() const;
	bool insert(const key_t&);
	bool contains(const key_t&) const;
	std::optional<key_t> next(const key_t&) const;
	std::optional<key_t> previous(const key_t&) const;
	void remove(const key_t&);
	bool empty() const;

	void diag(const key_t&);
private:
	void remove(const key_t&, size_t, size_t);
	std::bitset<storage_bits> bits_;
};

template <size_t N, template <class> class C>
template <class...O>
class set<N, C>::node::dense_impl<set<N, C>::node::min_rank, O...>::iterator {

};

template <size_t N, template <class> class C>
std::optional<typename set<N, C>::key_t> set<N, C>::first() const {
	return inner_.first();
}

template <size_t N, template <class> class C>
std::optional<typename set<N, C>::key_t> set<N, C>::last() const {
	return inner_.last();
}

template <size_t N, template <class> class C>
bool set<N, C>::insert(const key_t& key) {
	return inner_.insert(key);
}

template <size_t N, template <class> class C>
bool set<N, C>::contains(const key_t& key) const {
	return inner_.contains(key);
}

template <size_t N, template <class> class C>
std::optional<typename set<N, C>::key_t> set<N, C>::next(const key_t& key) const {
	return inner_.next(key);
}

template <size_t N, template <class> class C>
std::optional<typename set<N, C>::key_t> set<N, C>::previous(const key_t& key) const {
	return inner_.previous(key);
}

template <size_t N, template <class> class C>
void set<N, C>::remove(const key_t& key) {
	inner_.remove(key);
}

template <size_t N, template <class> class C>
bool set<N, C>::empty() const {
	return inner_.empty();
}

template <size_t N, template <class> class C>
void set<N, C>::diag(const key_t& key) {
	inner_.diag(key);
}

/* node methods */

template <size_t N, template <class> class C>
std::optional<typename set<N, C>::key_t> set<N, C>::node::first() const {
	return inner_.first();
}

template <size_t N, template <class> class C>
std::optional<typename set<N, C>::key_t> set<N, C>::node::last() const {
	return inner_.last();
}

template <size_t N, template <class> class C>
bool set<N, C>::node::insert(const key_t& key) {
	return inner_.insert(key);
}

template <size_t N, template <class> class C>
bool set<N, C>::node::contains(const key_t& key) const {
	return inner_.contains(key);
}

template <size_t N, template <class> class C>
std::optional<typename set<N, C>::key_t> set<N, C>::node::next(const key_t& key) const {
	return inner_.next(key);
}

template <size_t N, template <class> class C>
std::optional<typename set<N, C>::key_t> set<N, C>::node::previous(const key_t& key) const {
	return inner_.previous(key);
}

template <size_t N, template <class> class C>
void set<N, C>::node::remove(const key_t& key) {
	inner_.remove(key);
}

template <size_t N, template <class> class C>
bool set<N, C>::node::empty() const {
	return inner_.empty();
}

template <size_t N, template <class> class C>
void set<N, C>::node::diag(const key_t& key) {
	inner_.diag(key);
}

/* sparse_impl methods */

template <size_t N, template <class> class C>
template <size_t U, class...O>
 set<N, C>::node::sparse_impl<U, O...>::sparse_impl():contains_value_{false}, first_{}, last_{}, summary_{nullptr}, cluster_{} {

}

template <size_t N, template <class> class C>
template <size_t U, class...O>
std::optional<typename set<N, C>::key_t> set<N, C>::node::sparse_impl<U, O...>::first() const {
	return (contains_value_)? std::optional<key_t>(first_) : std::optional<key_t>();
}

template <size_t N, template <class> class C>
template <size_t U, class...O>
std::optional<typename set<N, C>::key_t> set<N, C>::node::sparse_impl<U, O...>::last() const {
	return (contains_value_)? std::optional<key_t>(last_) : std::optional<key_t>();
}

template <size_t N, template <class> class C>
template <size_t U, class...O>
void set<N, C>::node::sparse_impl<U, O...>::diag(const key_t& key) {
	std::cout << std::string(key.to_ullong(), '>') << " sparse impl (rank: " << U << ", size:" << sizeof(*this) << ")\n";
	if (!summary_) {
		summary_ = std::make_shared<summary_t>();
		summary_->diag(key.to_ullong() + 1);
	}
}

template <size_t N, template <class> class C>
template <size_t U, class...O>
bool set<N, C>::node::sparse_impl<U, O...>::insert(const key_t& key) {
	if (!contains_value_) {
		first_ = last_ = key;
		contains_value_ = true;
		return true;
	}

	C<key_t> comp;
	key_t inserted_key = key;
	if (comp(inserted_key, first_)) {
		auto temp = first_;
		first_ = inserted_key;
		inserted_key = temp;
	} else if (inserted_key == first_) {
		return false;
	}

	key_t high_bits = inserted_key >> cluster_bits;
	if (!cluster_.contains(high_bits) || cluster_[high_bits].empty()) {
		if (!summary_) {
			summary_ = std::make_shared<summary_t>();
		}
		summary_->insert(high_bits);
	}

	key_t low_mask = (~key_t()) >> (key_bits - cluster_bits);
	key_t low_bits = inserted_key & low_mask;
	bool result = cluster_[high_bits].insert(low_bits);
	if (result && comp(last_, inserted_key)) {
		last_ = inserted_key;
	}
	return result;
}

template <size_t N, template <class> class C>
template <size_t U, class...O>
bool set<N, C>::node::sparse_impl<U, O...>::contains(const key_t& key) const {

	if (!contains_value_) {
		return false;
	}
	if (first_ == key || last_ == key) {
		return true;
	}

	key_t high_bits = key >> cluster_bits;

	if (!cluster_.contains(high_bits)) {
		return false;
	}

	key_t low_mask = (~key_t()) >> (key_bits - cluster_bits);
	key_t low_bits = key & low_mask;
	return cluster_[high_bits].contains(low_bits);
}

template <size_t N, template <class> class C>
template <size_t U, class...O>
std::optional<typename set<N, C>::key_t> set<N, C>::node::sparse_impl<U, O...>::next(const key_t& key) const {
	if (!contains_value_) {
		return std::optional<key_t>();
	}

	C<key_t> comp;
	if (comp(key, first_)) {
		return std::optional<key_t>(first_);
	}

	key_t high_mask = (~key_t()) << cluster_bits;
	key_t high_bits = (key & high_mask) >> cluster_bits;
	if (cluster_.contains(high_bits)) {
		std::optional<key_t> last = cluster_[high_bits].last();
		key_t low_mask = (~key_t()) >> (key_bits - cluster_bits);
		if (last && comp(key & low_mask, *last)) {
			key_t low_bits = key & low_mask;
			std::optional<key_t> next = cluster_[high_bits].next(low_bits);
			
			return (next)? std::optional<key_t>((key & high_mask) | *next) : std::optional<key_t>();
		}
	}

	if (summary_) {
		std::optional<key_t> next = summary_->next(high_bits);
		if (next) {
			if (cluster_.contains(*next)) {
				std::optional<key_t> next_first = cluster_[*next].first();
				return (next_first)? std::optional<key_t>((*next << cluster_bits) | *next_first) : std::optional<key_t>();
			}
		}
	}

	return std::optional<key_t>();
}

template <size_t N, template <class> class C>
template <size_t U, class...O>
std::optional<typename set<N, C>::key_t> set<N, C>::node::sparse_impl<U, O...>::previous(const key_t& key) const {
	if (!contains_value_) {
		return std::optional<key_t>();
	}

	C<key_t> comp;
	if (comp(last_, key)) {
		return std::optional<key_t>(last_);
	}

	key_t high_mask = (~key_t()) << cluster_bits;
	key_t high_bits = (key & high_mask) >> cluster_bits;
	if (cluster_.contains(high_bits)) {
		std::optional<key_t> first = cluster_[high_bits].first();
		key_t low_mask = (~key_t()) >> (key_bits - cluster_bits);
		if (first && comp(*first, key & low_mask)) {
			key_t low_bits = key & low_mask;
			std::optional<key_t> previous = cluster_[high_bits].previous(low_bits);
			
			return (previous)? std::optional<key_t>((key & high_mask) | *previous) : std::optional<key_t>();
		}
	}

	if (summary_) {
		std::optional<key_t> previous = summary_->previous(high_bits);
		if (previous) {
			if (cluster_.contains(*previous)) {
				std::optional<key_t> previous_last = cluster_[*previous].last();
				return (previous_last)? std::optional<key_t>((*previous << cluster_bits) | *previous_last) : std::optional<key_t>();
			}
		}
	}

	if (comp(first_, key)) {
		return std::optional<key_t>(first_);
	}
	return std::optional<key_t>();
}

template <size_t N, template <class> class C>
template <size_t U, class...O>
void set<N, C>::node::sparse_impl<U, O...>::remove(const key_t& key) {
	if (!contains_value_) {
		return;
	}
	if (key == first_ && first_ == last_) {
		contains_value_ = false;
		return;
	}

	key_t removed_key = key;
	if (first_ == removed_key) {
		if (summary_) {
			auto first = summary_->first();
			if (first.has_value() && cluster_.contains(*first)) {
				auto first_min = cluster_[*first].first();
				removed_key = (*first << cluster_bits) | *first_min;
				first_ = removed_key;
			}
		}
	}
	
	key_t high_mask = (~key_t()) << cluster_bits;
	key_t high_bits = (key & high_mask) >> cluster_bits;

	if (cluster_.contains(high_bits)) {
		key_t low_mask = (~key_t()) >> (key_bits - cluster_bits);
		key_t low_bits = removed_key & low_mask;
		cluster_[high_bits].remove(low_bits);
		if (cluster_[high_bits].empty()) {
			if (summary_) {
				summary_->remove(high_bits);
				if (last_ == removed_key) {
					auto last = summary_->last();
					if (last.has_value()) {
						last_ = (*last << cluster_bits) | *cluster_[*last].last();
					} else {
						last_ = first_;
					}
				}
			}
		} else if (last_ == removed_key) {
			last_ = (high_bits << cluster_bits) | *cluster_[high_bits].last();
		}
	}
}

template <size_t N, template <class> class C>
template <size_t U, class...O>
bool set<N, C>::node::sparse_impl<U, O...>::empty() const {
	return !contains_value_;
}

/* sparse_impl specialization for min rank */

template <size_t N, template <class> class C>
template <class...O>
std::optional<typename set<N, C>::key_t> set<N, C>::node::sparse_impl<set<N, C>::node::min_rank, O...>::first() const {
	return (contains_value_)? std::optional<key_t>(first_) : std::optional<key_t>();
}

template <size_t N, template <class> class C>
template <class...O>
std::optional<typename set<N, C>::key_t> set<N, C>::node::sparse_impl<set<N, C>::node::min_rank, O...>::last() const {
	return (contains_value_)? std::optional<key_t>(last_) : std::optional<key_t>();
}

template <size_t N, template <class> class C>
template <class...O>
void set<N, C>::node::sparse_impl<set<N, C>::node::min_rank, O...>::diag(const key_t& key) {
	std::cout << std::string(key.to_ullong(), '>') << " sparse impl (rank: " << 
		set<N, C>::node::min_rank << ", size:" << sizeof(*this) << ")\n";
}

template <size_t N, template <class> class C>
template <class...O>
bool set<N, C>::node::sparse_impl<set<N, C>::node::min_rank, O...>::insert(const key_t& key) {
	if (!contains_value_) {
		first_ = last_ = key;
		contains_value_ = true;
		return true;
	}

	C<key_t> comp;
	bool result = false;
	auto inserted_key = key;
	if (comp(key, first_)) {
		inserted_key = first_;
		first_ = key;
		result = true;
	}

	if (comp(last_, inserted_key)) {
		last_ = inserted_key;
		result = true;
	}
	return result;
}

template <size_t N, template <class> class C>
template <class...O>
bool set<N, C>::node::sparse_impl<set<N, C>::node::min_rank, O...>::contains(const key_t& key) const {
	if (!contains_value_) {
		return false;
	}
	return first_ == key || last_ == key;
}

template <size_t N, template <class> class C>
template <class...O>
std::optional<typename set<N, C>::key_t> set<N, C>::node::sparse_impl<set<N, C>::node::min_rank, O...>::next(const key_t& key) const {
	if (empty()) {
		return std::optional<key_t>();
	}
	C<key_t> comp;
	if (comp(key, first_)) {
		return std::optional<key_t>(first_);
	}
	if (comp(key, last_)) {
		return std::optional<key_t>(last_);
	}
	return std::optional<key_t>();
}

template <size_t N, template <class> class C>
template <class...O>
std::optional<typename set<N, C>::key_t> set<N, C>::node::sparse_impl<set<N, C>::node::min_rank, O...>::previous(const key_t& key) const {
	if (empty()) {
		return std::optional<key_t>();
	}
	C<key_t> comp;
	if (comp(last_, key)) {
		return std::optional<key_t>(last_);
	}
	if (comp(first_, key)) {
		return std::optional<key_t>(first_);
	}
	return std::optional<key_t>();
}

template <size_t N, template <class> class C>
template <class...O>
void set<N, C>::node::sparse_impl<set<N, C>::node::min_rank, O...>::remove(const key_t& key) {
	if (!contains_value_) {
		return;
	}
	if (first_ == key && last_ == key) {
		contains_value_ = false;
		return;
	}
	if (first_ == key) {
		first_ = last_;
	}
	if (last_ == key) {
		last_ = first_;;
	}
}

template <size_t N, template <class> class C>
template <class...O>
bool set<N, C>::node::sparse_impl<set<N, C>::node::min_rank, O...>::empty() const {
	return !contains_value_;
}

/* dense_impl methods */

template <size_t N, template <class> class C>
template <size_t U, class...O>
set<N, C>::node::dense_impl<U, O...>::dense_impl():bits_{} {

}

template <size_t N, template <class> class C>
template <size_t U, class...O>
std::optional<typename set<N, C>::key_t> set<N, C>::node::dense_impl<U, O...>::first() const {
	if (!bits_[0]) {
		return std::optional<key_t>();
	}
	key_t result;
	for (size_t i = 0; i < key_bits; ++i) {
		result.set(i, bits_[1 + i]);
	}
	return std::optional<key_t>(result);
}

template <size_t N, template <class> class C>
template <size_t U, class...O>
std::optional<typename set<N, C>::key_t> set<N, C>::node::dense_impl<U, O...>::last() const {
	if (!bits_[0]) {
		return std::optional<key_t>();
	}
	key_t result;
	size_t offset = 1 + key_bits;
	for (size_t i = 0; i < key_bits; ++i) {
		result.set(i, bits_[offset + i]);
	}
	return std::optional<key_t>(result);
}

template <size_t N, template <class> class C>
template <size_t U, class...O>
void set<N, C>::node::dense_impl<U, O...>::diag(const key_t& key) {
	std::cout << std::string(key.to_ullong(), '>') << " dense impl (rank: " << U << ", size:" << sizeof(*this) << ")\n";
}

template <size_t N, template <class> class C>
template <size_t U, class...O>
bool set<N, C>::node::dense_impl<U, O...>::insert(const key_t& key) {
	return insert(key, key_bits, 0);
}

template <size_t N, template <class> class C>
template <size_t U, class...O>
bool set<N, C>::node::dense_impl<U, O...>::insert(const key_t& key, size_t cur_bits, size_t storage_offset) {
	if (!bits_[storage_offset]) {
		bits_[storage_offset] = true;
		size_t offset = 1 + storage_offset;
		for (size_t i=0; i<cur_bits; ++i) {
			bits_[offset + i] = key[i];
		}
		offset += cur_bits;
		for (size_t i=0; i<cur_bits; ++i) {
			bits_[offset + i] = key[i];
		}
		return true;
	}

	C<bool> comp;
	key_t cur_key = key;
	bool equal_to_first = true;
	for (size_t i = 1; i <= cur_bits; ++i) {
		size_t k = cur_bits - i, s = 1 + cur_bits - i + storage_offset;
		if (cur_key[k] ^ bits_[s]) {
			equal_to_first = false;
			if (comp(cur_key[k], bits_[s])) {
				cur_key.flip(k);
				bits_.flip(s);
				while (++i <= cur_bits) {
					if (cur_key[--k] ^ bits_[--s]) {
						bool temp = cur_key[k];
						cur_key.set(k, bits_[s]);
						bits_.set(s, temp);
					}
				}
			} else {
				break;
			}
		}
	}
	if (equal_to_first) {
		return false;
	}

	bool result = false;
	if (cur_bits > node::min_rank) {
		size_t cur_summary_bits = ceil(cur_bits * 0.5);
		size_t cur_cluster_bits = floor(cur_bits * 0.5);
		size_t cur_summary_storage_bits = storage_bits[cur_summary_bits];
		size_t cur_cluster_storage_bits = storage_bits[cur_cluster_bits];
		key_t high_key = cur_key >> cur_cluster_bits;
		size_t summary_offset = storage_offset + 1 + 2 * cur_bits;
		size_t cluster_offset = summary_offset + cur_summary_storage_bits + cur_cluster_storage_bits * high_key.to_ulong();
		size_t low_shift = node::key_bits - cur_cluster_bits;
		auto low_key = (cur_key << low_shift) >> low_shift;
		if (!bits_[cluster_offset]) {
			insert(high_key, cur_summary_bits, summary_offset);
		}
		result = insert(low_key, cur_cluster_bits, cluster_offset);
	}

	if (result || cur_bits == node::min_rank) {
		for (size_t i = 1; i <= cur_bits; ++i) {
			size_t k = cur_bits - i, s = 1 + 2 * cur_bits - i + storage_offset;
			if (cur_key[k] ^ bits_[s]) {
				result = true;
				if (comp(bits_[s], cur_key[k])) {
					bits_.flip(s);
					while (++i <= cur_bits) {
						if (cur_key[--k] ^ bits_[--s]) {
							bits_.set(s, cur_key[k]);
						}
					}
				} else {
					break;
				}
			}
		}
	}

	return result;
}

template <size_t N, template <class> class C>
template <size_t U, class...O>
bool set<N, C>::node::dense_impl<U, O...>::contains(const key_t& searched_key) const {
	return contains(searched_key, U, 0);
}

template <size_t N, template <class> class C>
template <size_t U, class...O>
bool set<N, C>::node::dense_impl<U, O...>::contains(const key_t& searched_key, size_t cur_bits, size_t offset) const {
	if (!bits_[offset]) {
		return false;
	}
	key_t key = searched_key;
	key_t mask = ~key_t();

	while (cur_bits >= node::min_rank) {
		key_t value;
		bool found = true;
		for (size_t i = 0; i < cur_bits; ++i) {
			if (key[i] ^ bits_[offset + i + 1]) {
				found = false;
				break;
			}
		}
		if (!found) {
			found = true;
			for (size_t i = 0; i < cur_bits; ++i) {
				if (key[i] ^ bits_[offset + i + cur_bits + 1]) {
					found = false;
					break;
				}
			}
		}
		if (found) {
			return true;
		}

		double cur_half_bits = cur_bits * 0.5;
		size_t cur_summary_bits = ceil(cur_half_bits);
		size_t cur_cluster_bits = floor(cur_half_bits);

		key_t high_key = key >> cur_cluster_bits;
		offset += 1 + 2 * cur_bits + storage_bits[cur_summary_bits] + storage_bits[cur_cluster_bits] * high_key.to_ulong();
		if (!bits_[offset]) {
			return false;
		}

		cur_bits = cur_cluster_bits;
		key <<= (node::key_bits - cur_cluster_bits);
		key >>= (node::key_bits - cur_cluster_bits);
	}
	return false;
}

template <size_t N, template <class> class C>
template <size_t U, class...O>
std::optional<typename set<N, C>::key_t> set<N, C>::node::dense_impl<U, O...>::next(const key_t& key) const {
	return next(key, key_bits, 0);
}

template <size_t N, template <class> class C>
template <size_t U, class...O>
std::optional<typename set<N, C>::key_t> set<N, C>::node::dense_impl<U, O...>::next(const key_t& key, size_t cur_bits, size_t storage_offset) const {
	if (!bits_[storage_offset]) {
		return std::optional<key_t>();
	}
	
	key_t cur_key = key;
	C<int> comp;
	for (size_t i = 1; i <= cur_bits; ++i) {
		size_t k = cur_bits - i, s = 1 + cur_bits - i + storage_offset;
		if (cur_key[k] ^ bits_[s]) {
			if (comp(cur_key[k], bits_[s])) {
				cur_key.flip(k);
				while (++i <= cur_bits) {
					cur_key.set(--k, bits_[--s]);
				}
				return std::optional<key_t>(cur_key);
			} else {
				break;
			}
		}
	}

	if (cur_bits > node::min_rank) {
		size_t cur_summary_bits = ceil(cur_bits * 0.5);
		size_t cur_cluster_bits = floor(cur_bits * 0.5);
		size_t cur_summary_storage_bits = storage_bits[cur_summary_bits];
		size_t cur_cluster_storage_bits = storage_bits[cur_cluster_bits];
		key_t high_key = cur_key >> cur_cluster_bits;
		size_t summary_offset = storage_offset + 1 + 2 * cur_bits;
		size_t cluster_offset = summary_offset + cur_summary_storage_bits + cur_cluster_storage_bits * high_key.to_ulong();

		if (bits_[cluster_offset]) {
			size_t low_shift = node::key_bits - cur_cluster_bits;
			auto low_key = (cur_key << low_shift) >> low_shift;
			for (size_t i = 1; i <= cur_cluster_bits; ++i) {
				size_t k = cur_cluster_bits - i, s = 1 + 2 * cur_cluster_bits - i + cluster_offset;
				if (low_key[k] ^ bits_[s]) {
					if (comp(low_key[k], bits_[s])) {
						auto n = next(low_key, cur_cluster_bits, cluster_offset);
						return (n)? std::optional<key_t>((high_key << cur_cluster_bits) | *n) : n;
					} else {
						break;
					}
				}
			}
		}

		if (bits_[summary_offset]) {
			auto n = next(high_key, cur_summary_bits, summary_offset);
			if (n) {
				cluster_offset = summary_offset + cur_summary_storage_bits + cur_cluster_storage_bits * n->to_ulong();
				if (bits_[cluster_offset]) {
					*n <<= cur_cluster_bits;
					for (size_t i = 0; i < cur_cluster_bits; ++i) {
						n->set(i, bits_[1 + cluster_offset + i]);
					}
					return n;
				}
			}
		}
	} else {
		for (size_t i = 1; i <= cur_bits; ++i) {
			size_t k = cur_bits - i, s = 1 + 2 * cur_bits - i + storage_offset;
			if (cur_key[k] ^ bits_[s]) {
				if (comp(cur_key[k], bits_[s])) {
					cur_key.flip(k);
					while (++i <= cur_bits) {
						cur_key.set(--k, bits_[--s]);
					}
					return std::optional<key_t>(cur_key);
				} else {
					break;
				}
			}
		}
	}

	return std::optional<key_t>();
}

template <size_t N, template <class> class C>
template <size_t U, class...O>
std::optional<typename set<N, C>::key_t> set<N, C>::node::dense_impl<U, O...>::previous(const key_t& key) const {
	return previous(key, key_bits, 0);
}

template <size_t N, template <class> class C>
template <size_t U, class...O>
std::optional<typename set<N, C>::key_t> set<N, C>::node::dense_impl<U, O...>::previous(const key_t& key, size_t cur_bits, size_t storage_offset) const {
	if (!bits_[storage_offset]) {
		return std::optional<key_t>();
	}
	size_t cur_summary_bits = ceil(cur_bits * 0.5);
	size_t cur_cluster_bits = floor(cur_bits * 0.5);
	size_t cur_summary_storage_bits = storage_bits[cur_summary_bits];
	size_t cur_cluster_storage_bits = storage_bits[cur_cluster_bits];
	key_t cur_key = key;
	C<int> comp;
	for (size_t i = 1; i <= cur_bits; ++i) {
		size_t k = cur_bits - i, s = 1 + 2 * cur_bits - i + storage_offset;
		if (cur_key[k] ^ bits_[s]) {
			if (comp(bits_[s], cur_key[k])) {
				cur_key.flip(k);
				while (++i <= cur_bits) {
					cur_key.set(--k, bits_[--s]);
				}
				return std::optional<key_t>(cur_key);
			} else {
				break;
			}
		}
	}

	if (cur_bits > node::min_rank) {
		key_t high_key = cur_key >> cur_cluster_bits;
		size_t summary_offset = storage_offset + 1 + 2 * cur_bits;
		size_t cluster_offset = summary_offset + cur_summary_storage_bits + cur_cluster_storage_bits * high_key.to_ulong();

		if (bits_[cluster_offset]) {
			size_t low_shift = node::key_bits - cur_cluster_bits;
			auto low_key = (cur_key << low_shift) >> low_shift;
			for (size_t i = 1; i <= cur_cluster_bits; ++i) {
				size_t k = cur_cluster_bits - i, s = 1 + cur_cluster_bits - i + cluster_offset;
				if (low_key[k] ^ bits_[s]) {
					if (comp(bits_[s], low_key[k])) {
						auto prev = previous(low_key, cur_cluster_bits, cluster_offset);
						return (prev)? std::optional<key_t>((high_key << cur_cluster_bits) | *prev) : prev;
					} else {
						break;
					}
				}
			}
		}

		if (bits_[summary_offset]) {
			auto prev = previous(high_key, cur_summary_bits, summary_offset);
			if (prev) {
				cluster_offset = summary_offset + cur_summary_storage_bits + cur_cluster_storage_bits * prev->to_ulong();
				if (bits_[cluster_offset]) {
					*prev <<= cur_cluster_bits;
					for (size_t i = 0; i < cur_cluster_bits; ++i) {
						prev->set(i, bits_[1 + cur_cluster_bits + cluster_offset + i]);
					}
					return prev;
				}
			}
		}
	}

	for (size_t i = 1; i <= cur_bits; ++i) {
		size_t k = cur_bits - i, s = 1 + cur_bits - i + storage_offset;
		if (cur_key[k] ^ bits_[s]) {
			if (comp(bits_[s], cur_key[k])) {
				cur_key.flip(k);
				while (++i <= cur_bits) {
					cur_key.set(--k, bits_[--s]);
				}
				return std::optional<key_t>(cur_key);
			} else {
				break;
			}
		}
	}

	return std::optional<key_t>();
}

template <size_t N, template <class> class C>
template <size_t U, class...O>
void set<N, C>::node::dense_impl<U, O...>::remove(const key_t& key) {
	remove(key, key_bits, 0);
}

template <size_t N, template <class> class C>
template <size_t U, class...O>
void set<N, C>::node::dense_impl<U, O...>::remove(const key_t& key, size_t cur_bits, size_t storage_offset) {
	if (!bits_[storage_offset]) {
		return;
	}

	if (cur_bits > node::min_rank) {
		bool equals_first = true;
		for (size_t i=0; i<cur_bits; ++i) {
			if (key[i] ^ bits_[1 + storage_offset + i]) {
				equals_first = false;
				break;
			}
		}

		key_t removed_key = key;
		size_t cur_summary_bits = ceil(cur_bits * 0.5);
		size_t cur_cluster_bits = floor(cur_bits * 0.5);
		size_t summary_offset = 1 + 2 * cur_bits + storage_offset;

		if (equals_first) {
			bool equals_last = true;
			for (size_t i=0; i<cur_bits; ++i) {
				if (removed_key[i] ^ bits_[1 + storage_offset + cur_bits + i]) {
					equals_last = false;
					break;
				}
			}
			if (equals_last) {
				bits_[storage_offset] = false;
				return;
			}

			if (bits_[summary_offset]) {
				key_t summary_first;
				for (size_t i=0; i<cur_summary_bits; ++i) {
					summary_first.set(i, bits_[1 + summary_offset + i]);
				}
				
				size_t cluster_offset = summary_offset + storage_bits[cur_summary_bits] + storage_bits[cur_cluster_bits] * summary_first.to_ulong();
				if (bits_[cluster_offset]) {
					removed_key = summary_first << cur_cluster_bits;
					for (size_t i=0; i<cur_cluster_bits; ++i) {
						bool temp = bits_[1 + cluster_offset + i];
						removed_key.set(i, temp);
						bits_.set(1 + i + storage_offset, temp);
					}
					for (size_t i=cur_cluster_bits; i<cur_bits; ++i) {
						bits_.set(1 + i + storage_offset, removed_key[i]);
					}
				}
			}
		}
		
		key_t high_bits = removed_key >> cur_cluster_bits;
		size_t cluster_offset = summary_offset + storage_bits[cur_summary_bits] + storage_bits[cur_cluster_bits] * high_bits.to_ulong();
		if (bits_[cluster_offset]) {
			size_t low_shift = node::key_bits - cur_cluster_bits;
			key_t low_bits = (removed_key << low_shift) >> low_shift;
			remove(low_bits, cur_cluster_bits, cluster_offset);
			if (!bits_[cluster_offset]) {
				remove(high_bits, cur_summary_bits, summary_offset);
				for (size_t i=0; i<cur_bits; ++i) {
					if (bits_[1 + cur_bits + storage_offset + i] ^ removed_key[i]) {
						return;
					}
				}
				if (bits_[summary_offset]) {
					key_t summary_last;
					for (size_t i=0; i<cur_summary_bits; ++i) {
						bits_[1 + storage_offset + cur_bits + cur_cluster_bits + i] = 
							summary_last[i] = 
							bits_[1 + summary_offset + cur_summary_bits + i];
					}
					cluster_offset = summary_offset + 
						storage_bits[cur_summary_bits] + 
						storage_bits[cur_cluster_bits] * summary_last.to_ulong();
					for (size_t i=0; i<cur_cluster_bits; ++i) {
						bits_[1 + storage_offset + cur_bits + i] = bits_[1 + cluster_offset + cur_cluster_bits + i];
					}
				} else {
					for (size_t i=1 + storage_offset; i<1 + storage_offset + cur_bits; ++i) {
						bits_[i + cur_bits] = bits_[i];
					}
				}
			} else {
				for (size_t i=0; i<cur_bits; ++i) {
					if (bits_[1 + storage_offset + cur_bits + i] ^ removed_key[i]) {
						return;
					}
				}
				for (size_t i=0; i<cur_summary_bits; ++i) {
					bits_[1 + storage_offset + cur_bits + cur_cluster_bits + i] = high_bits[i];
				}
				for (size_t i=0; i<cur_cluster_bits; ++i) {
					bits_[1 + storage_offset + cur_bits + i] = bits_[1 + cluster_offset + cur_cluster_bits + i];
				}
			}
		}
	} else {
		if (bits_[1 + storage_offset] == key[0]) {
			if (bits_[2 + storage_offset] == key[0]) {
				bits_[storage_offset] = false;
				return;
			}
			bits_[1 + storage_offset] = bits_[2 + storage_offset];
		} else if (bits_[2 + storage_offset] == key[0]) {
			bits_[2 + storage_offset] = bits_[1 + storage_offset];
		}
	}
}

template <size_t N, template <class> class C>
template <size_t U, class...O>
bool set<N, C>::node::dense_impl<U, O...>::empty() const {
	return !bits_[0];
}

/* dense_impl specialization for min rank */

template <size_t N, template <class> class C>
template <class...O>
std::optional<typename set<N, C>::key_t> set<N, C>::node::dense_impl<set<N, C>::node::min_rank, O...>::first() const {
	return bits_[0]? std::optional<key_t>(bits_[1]): std::optional<key_t>();
}

template <size_t N, template <class> class C>
template <class...O>
std::optional<typename set<N, C>::key_t> set<N, C>::node::dense_impl<set<N, C>::node::min_rank, O...>::last() const {
	return bits_[0]? std::optional<key_t>(bits_[2]): std::optional<key_t>();
}

template <size_t N, template <class> class C>
template <class...O>
void set<N, C>::node::dense_impl<set<N, C>::node::min_rank, O...>::diag(const key_t& key) {
	std::cout << std::string(key.to_ullong(), '>') << " dense impl (rank: " << set<N, C>::node::min_rank << ", size:" << sizeof(*this) << ")\n";
}

template <size_t N, template <class> class C>
template <class...O>
bool set<N, C>::node::dense_impl<set<N, C>::node::min_rank, O...>::insert(const key_t& key) {
	if (!bits_[0]) {
		bits_.set(0);
		bits_[1] = bits_[2] = key[0];
		return true;
	}
	C<unsigned char> comp;
	if (comp(key[0], bits_[1])) {
		bits_.set(1, key[0]);
		return true;
	}
	if (comp(bits_[2], key[0])) {
		bits_.set(2, key[0]);
		return true;
	}
	return false;
}

template <size_t N, template <class> class C>
template <class...O>
bool set<N, C>::node::dense_impl<set<N, C>::node::min_rank, O...>::contains(const key_t& key) const {
	if (!bits_[0]) {
		return false;
	}
	return bits_[1] == key[0] || bits_[2] == key[0];
}

template <size_t N, template <class> class C>
template <class...O>
std::optional<typename set<N, C>::key_t> set<N, C>::node::dense_impl<set<N, C>::node::min_rank, O...>::next(const key_t& key) const {
	return bits_[0] && C<unsigned char>()(key[0], bits_[2]) ? std::optional<key_t>(bits_[2]) : std::optional<key_t>();
}

template <size_t N, template <class> class C>
template <class...O>
std::optional<typename set<N, C>::key_t> set<N, C>::node::dense_impl<set<N, C>::node::min_rank, O...>::previous(const key_t& key) const {
	return bits_[0] && C<unsigned char>()(bits_[1], key[0]) ? std::optional<key_t>(bits_[1]) : std::optional<key_t>();
}

template <size_t N, template <class> class C>
template <class...O>
void set<N, C>::node::dense_impl<set<N, C>::node::min_rank, O...>::remove(const key_t& key) {
	return remove(key, 1, 0);
}

template <size_t N, template <class> class C>
template <class...O>
void set<N, C>::node::dense_impl<set<N, C>::node::min_rank, O...>::remove(const key_t& key, size_t cur_bits, size_t storage_offset) {
	if (!bits_[storage_offset]) {
		return;
	}
	if (bits_[1 + storage_offset] == key[0]) {
		if (bits_[2 + storage_offset] == key[0]) {
			bits_[storage_offset] = false;
			return;
		}
	} else {
		if (bits_[2 + storage_offset] == key[0]) {
			bits_[2 + storage_offset] = bits_[1 + storage_offset];
		}
	}
}

template <size_t N, template <class> class C>
template <class...O>
bool set<N, C>::node::dense_impl<set<N, C>::node::min_rank, O...>::empty() const {
	return !bits_[0];
}

/* sparse_impl::cluster_storage implementations */

template <size_t N, template <class> class C>
template <size_t U, class...O>
template <class T>
class set<N, C>::node::sparse_impl<U, O...>::cluster_storage {
	class hash_impl;
	typename utils::select_impl<hash_impl>::type impl_;
public:
	bool contains(const key_t& key) const { return impl_.contains(key); }
	T& operator[](const key_t& key) { return impl_[key]; }
	const T& operator[](const key_t& key) const { return impl_[key]; }
};

template <size_t N, template <class> class C>
template <size_t U, class...O>
template <class T>
class set<N, C>::node::sparse_impl<U, O...>::cluster_storage<T>::hash_impl {
	template <class I> class num_impl;
	class bitset_impl;

	typename utils::select_impl<
		num_impl<unsigned char>, 
		num_impl<unsigned short>, 
		num_impl<unsigned>,
		num_impl<unsigned long>,
		bitset_impl>::type impl_;
public:
	static constexpr bool is_enabled = true;

	bool contains(const key_t& key) const { return impl_.contains(key); }
	T& operator[](const key_t& key) { return impl_[key]; }
	const T& operator[](const key_t& key) const { return impl_[key]; }
};


template <size_t N, template <class> class C>
template <size_t U, class...O>
template <class T>
template <class I>
class set<N, C>::node::sparse_impl<U, O...>::cluster_storage<T>::hash_impl::num_impl {
public:
	static constexpr bool is_enabled = set<N, C>::node::key_bits <= std::numeric_limits<I>::digits;

	bool contains(const key_t& key) const { return data_.find(key.to_ulong()) != data_.end(); }
	T& operator[](const key_t& key) { return data_[key.to_ulong()]; }
	const T& operator[](const key_t& key) const { return data_.at(key.to_ulong()); }
private:
	std::unordered_map<I, T> data_;
};

template <size_t N, template <class> class C>
template <size_t U, class...O>
template <class T>
class set<N, C>::node::sparse_impl<U, O...>::cluster_storage<T>::hash_impl::bitset_impl {
public:
	static constexpr bool is_enabled = true;

	bool contains(const key_t& key) const { return data_.find(key) != data_.end(); }
	T& operator[](const key_t& key) { return data_[key]; }
	const T& operator[](const key_t& key) const { return data_.at(key); }
private:
	std::unordered_map<key_t, T> data_;
};

/* implementation selection helpers */

template <size_t N, template <class> class C>
template <size_t U, class...O>
class set<N, C>::node::average_size_of_sparse_impl {
	static constexpr size_t cluster_size = sparse_impl<U, O...>::summary_size * 0.5;
	typename sparse_impl<U, O...>::summary_t summary_;
	key_t first_, last_;
	std::array<typename sparse_impl<U, O...>::cluster_t, cluster_size> cluster_;
};

template <size_t N, template <class> class C>
template <class...O>
class set<N, C>::node::average_size_of_sparse_impl<set<N, C>::node::min_rank, O...> {
	sparse_impl<min_rank, O...> inner_;
};

template <size_t N, template <class> class C>
template <size_t U, class...O>
class set<N, C>::node::average_size_of_dense_impl {
	typename dense_impl<U, O...>::bits_t bits_;
};

template <size_t N, template <class> class C>
template <class...O>
class set<N, C>::node::average_size_of_dense_impl<set<N, C>::node::min_rank, O...> {
	dense_impl<min_rank, O...> inner_;
};

}



#endif
