#include <veb/set.hpp>
#include <chrono>


template <size_t u, size_t v>
void test_vebset() {
	std::chrono::high_resolution_clock clock;
	auto tp1 = clock.now();
	// veb::set<u * v> x;
	veb::set<std::numeric_limits<size_t>::max()> x;
	x.diag(0);

	for (size_t i = 1; i < u; ++i) {
		x.insert(i * v);
	}
	size_t remove_every = 7;
	size_t removed = 0;
	for (size_t i = 0; i < u; i += remove_every) {
		x.remove(i * v);
		++removed;
	}
	auto tp2 = clock.now();
	std::cout << "insert: " << std::chrono::duration_cast<std::chrono::milliseconds>(tp2-tp1).count();
	

	// auto i = x.last();
	// size_t c = 1;

	// size_t previous = i->to_ulong();
	// while (i.has_value()) {
 // 		i = x.previous(*i);
 // 		if (i.has_value()) {
 // 			if (previous < i->to_ulong()) {
 // 				std::cout << "error\n";
 // 				return;
 // 			}
 // 			previous = i->to_ulong();
 // 		}
 // 		++c;
	// }

	auto i = x.first();
	size_t c = 1;

	size_t previous = i->to_ulong();
	while (i.has_value()) {
 		i = x.next(*i);
 		if (i.has_value()) {
 			if (previous > i->to_ulong()) {
 				std::cout << "error\n";
 				return;
 			}
 			previous = i->to_ulong();
 		}
 		++c;
	}
	auto tp3 = clock.now();
	std::cout << ", iter: " << c << " " << std::chrono::duration_cast<std::chrono::milliseconds>(tp3-tp2).count();

	c = 0;
	tp1 = clock.now();
	for (size_t i = 1; i < u * v; ++i) {
		if (x.contains(i)) {
			++c;
			if (i % v != 0) {
				std::cout << "contains " << i << "\n";
				break;
			}
		} else {
			if (i % v == 0 && i % remove_every != 0) {
				std::cout << "does not contain " << i << "\n";
				break;
			}
		}
	}
	tp2 = clock.now();

	std::cout << ", contains: " << c << " removed: " << removed 
		<< " " << std::chrono::duration_cast<std::chrono::milliseconds>(tp2-tp1).count() << "\n";
}

int main() {

	// test_vebset<50, 3>();
	// test_vebset<50, 3>();

	// test_vebset<10000000, 45671>();
	// test_vebset<10000000, 11>();
	test_vebset<5000000, 41>();
	test_vebset<5000000, 11>();

	return EXIT_SUCCESS;
};